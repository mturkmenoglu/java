package oop.interfaceExample.ex001;

@SuppressWarnings("all")
public interface Shape {
    public float area();
    public float volume();
    public String getName();
}
